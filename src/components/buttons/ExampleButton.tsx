import { bool, func, oneOf, string } from 'prop-types';
import React, { ButtonHTMLAttributes, FunctionComponent, RefAttributes } from 'react';
import './button.css';

export type ExampleButtonProps = {
  primary?: boolean,
  backgroundColor?: string,
  size?: 'small' | 'medium' | 'large',
  label: string
} & ButtonHTMLAttributes<HTMLButtonElement>
  & RefAttributes<HTMLButtonElement>

/**
 * Primary UI component for user interaction
 */
export const ExampleButton: FunctionComponent<ExampleButtonProps> = (props) => {
  const { primary, backgroundColor, size, label, ...rest } = props;

  const mode = primary ? 'storybook-button--primary' : 'storybook-button--secondary';
  return (
    <button
      type="button"
      className={['storybook-button', `storybook-button--${size}`, mode].join(' ')}
      style={backgroundColor && { backgroundColor }}
      {...rest}
    >
      {label}
    </button>
  );
};
export default ExampleButton

ExampleButton.propTypes = {
  /**
   * Is this the principal call to action on the page?
   */
  primary: bool,
  /**
   * What background color to use
   */
  backgroundColor: string,
  /**
   * How large should the button be?
   */
  size: oneOf(['small', 'medium', 'large']),
  /**
   * Button contents
   */
  label: string.isRequired,
  /**
   * Optional click handler
   */
  onClick: func,
  /**
   * Children is required. EXAMPLE
   */
  // children: oneOfType([element]).isRequired
}

ExampleButton.defaultProps = {
  backgroundColor: null,
  primary: false,
  size: 'medium',
  onClick: undefined,
};
