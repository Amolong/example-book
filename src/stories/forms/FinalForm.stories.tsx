import { CardContent } from "@material-ui/core";
import { Meta, Story } from "@storybook/react";
import React from "react";
import { Field } from "react-final-form";
import DemoForm from "./decorators/DemoForm";

const metadata: Meta = {
  title: 'Forms/FinalForm',
  decorators: [DemoForm]
}
export default metadata;

export const Simple: Story = (args, context) => {
  return (
    <CardContent>
      <Field name='name' component='input' />
    </CardContent>
  )
}
Simple.storyName = 'Simple FinalForm'