import { Card, CardContent, Divider, Typography } from '@material-ui/core'
import { DecoratorFn, Parameters } from '@storybook/react'
import React, { FunctionComponent } from 'react'
import { Form, FormSpy } from 'react-final-form'
import ReactJson from 'react-json-view'

/**
 * Either a custom `onSubmit` function or a static.
 * @param context The story context to possibly get the `onSubmit` function from
 * @returns A submit function
 */
const getSubmit = (params: Parameters) => {
  const { onSubmit } = params

  return typeof onSubmit === 'function' ?
    onSubmit :
    () => alert('Form submitted')
}

/**
 * A Json representation of the form.
 * @returns An interactive Json representation of the current form
 *
 * @see https://github.com/mac-s-g/react-json-view
 */
const FormJson: FunctionComponent<{}> = () => {
  return (
    <FormSpy>
      {formProps => (
        <ReactJson
          src={formProps}
          theme='monokai'
          collapsed={1} />
      )}
    </FormSpy>
  )
}

const DemoForm: DecoratorFn = (story, context) => {
  const { parameters } = context;

  return (
    <Form onSubmit={getSubmit(parameters)}>
      {({ handleSubmit }) => (
        <>
          {/* Provide story in a form */}
          <form onSubmit={handleSubmit}>
            {story()}
          </form>
          {/* Extend the story with a form spy */}
          <Divider />
          <CardContent>
            <Typography variant='body2' color="textSecondary">FormSpy for the DemoForm</Typography>
            <FormJson />
          </CardContent>
        </>
      )}
    </Form>
  )
}
export default DemoForm