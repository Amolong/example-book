import { CardContent } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import React from 'react';
import ExampleButton, { ExampleButtonProps } from '../../components/buttons/ExampleButton';

const metadata: Meta = {
  title: 'Buttons/ExampleButtons',
  component: ExampleButton,
  argTypes: {
    backgroundColor: {
      control: 'color',
      description: 'Background color overrides Primary color if set.'
    }
  },
};
export default metadata

const Template: Story<ExampleButtonProps> = (args) => (
  <CardContent>
    <ExampleButton {...args} />
  </CardContent>
);

export const Primary: Story = Template.bind(this);
Primary.args = {
  primary: true,
  label: 'Primary',
};

export const Secondary: Story = Template.bind({});
Secondary.args = {
  label: 'Secondary',
};

export const Large: Story = Template.bind({});
Large.args = {
  size: 'large',
  label: 'Large',
};

export const Small: Story = Template.bind({});
Small.args = {
  size: 'small',
  label: 'Small',
};