import { Meta, Story } from "@storybook/react";
import React from "react";

/**
 * @see https://github.com/greena13/react-hotkeys
 */
const metadata: Meta = {
  title: 'Hotkeys/ReactHotkey'
}
export default metadata;

export const Simple: Story = (args, context) => {
  return (
    <>
    </>
  )
}
Simple.storyName = 'Simple ReactHotkey'