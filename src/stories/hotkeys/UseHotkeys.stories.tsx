import { Button, CardActions, CardContent, Typography } from '@material-ui/core';
import { actions } from '@storybook/addon-actions';
import { Meta, Story } from "@storybook/react";
import React, { useRef } from "react";
import { useHotkeys } from "react-hotkeys-hook";

const metadata: Meta = {
  title: 'Hotkeys/useHotkeys',
  decorators: []
}
export default metadata;

const buttonActions = actions('onClick', 'onFocus', { clearOnStoryChange: true });

export const OnFocus: Story = (args, context) => {
  const firstRef = useHotkeys<HTMLButtonElement>('ctrl+enter', () => {
    console.log('firstRef', firstRef.current, firstRef.current?.click())
  });
  const secondRef = useHotkeys<HTMLButtonElement>('ctrl+enter', () => {
    console.log('secondRef', secondRef.current, secondRef.current?.click())
  });

  return (
    <CardContent>
      <CardActions>
        <button ref={firstRef} {...buttonActions}>First</button>
        <button ref={secondRef} {...buttonActions}>Second</button>
      </CardActions>
    </CardContent>
  )
}

export const OnParentFocus: Story = (args, context) => {
  const btnRef = useRef<HTMLButtonElement>()
  const divRef = useHotkeys<HTMLDivElement>('ctrl+enter', () => {
    divRef.current && btnRef.current && btnRef.current.click()
  })

  return (
    <CardContent>
      <Typography variant='body2' color="textSecondary">
        The div (Card &gt; CardContent &gt; Typography) is the parent which should have focus which causes the hotkey 'ctrl+enter' to trigger button onClick
      </Typography>
      <CardActions>
        <Button color='primary' ref={btnRef} {...buttonActions}>First</Button>
      </CardActions>
    </CardContent>
  )
}