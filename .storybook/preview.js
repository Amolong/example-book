import CardDecorator from "./decorators/CardDecorator"

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}

export const decorators = [CardDecorator]