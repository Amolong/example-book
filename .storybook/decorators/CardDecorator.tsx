import { Card, CardContent } from '@material-ui/core'
import { DecoratorFn } from '@storybook/react'
import React from 'react'

const CardDecorator: DecoratorFn = (story, context) => {
  return (
    <Card>
      {story()}
      {/* <CardContent>
      </CardContent> */}
    </Card>
  )
}
export default CardDecorator